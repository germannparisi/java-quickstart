package ar.edu.untref.aydoo.unit;

import ar.edu.untref.aydoo.Calculadora;
import org.junit.Assert;
import org.junit.Test;

public class CalculadoraTest {

    @Test
    public void chequearSumaNumerosChicos()
    {
        Calculadora calculadora = new Calculadora();
        int result = calculadora.sumar(1, 2);
        Assert.assertEquals(3, result);
    }
    
    @Test
    public void chequearSumaNumerosGrandes()
    {
        Calculadora calculadora = new Calculadora();
        int result = calculadora.sumar(15000, 16000);
        Assert.assertEquals(31000, result);
    }
    
}
