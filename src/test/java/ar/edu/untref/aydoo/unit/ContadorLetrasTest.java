package ar.edu.untref.aydoo.unit;

import ar.edu.untref.aydoo.ContadorLetras;
import java.util.HashMap;
import java.util.Map;
import org.junit.Assert;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

public class ContadorLetrasTest {

    @Test
    public void chequearCadenaVacia() {
        ContadorLetras contadorLetras = new ContadorLetras();
        String s = "";
        HashMap<Character, Integer> result = contadorLetras.contar(s);
        assertTrue(result.isEmpty());
    }

    @Test
    public void chequearCadenaDeUnCaracter() {
        ContadorLetras contadorLetras = new ContadorLetras();
        String s = "a";
        HashMap<Character, Integer> result = contadorLetras.contar(s);
        int cantidad = result.get('a');
        Assert.assertEquals(1, cantidad);
        Assert.assertEquals(1, result.size());
//        
//        for (Map.Entry<Character, Integer> elemento : result.entrySet()) {
//            Character key = elemento.getKey();
//            Integer value = elemento.getValue();
//            
//            
//
//            // do what you have to do here
//            // In your case, another loop.
//        }

        
    }
    
    @Test
    public void chequearCadenaVariosCaracteres() {
        ContadorLetras contadorLetras = new ContadorLetras();
        String s = "notebook";
        HashMap<Character, Integer> result = contadorLetras.contar(s);
        int cantidadDeN = result.get('n');
        int cantidadDeO = result.get('o');
        int cantidadDeT = result.get('t');
        int cantidadDeE = result.get('e');
        int cantidadDeB = result.get('b');
        int cantidadDeK = result.get('k');
        
        Assert.assertEquals(6, result.size());
        Assert.assertEquals(1, cantidadDeN);
        Assert.assertEquals(3, cantidadDeO);
        Assert.assertEquals(1, cantidadDeT);
        Assert.assertEquals(1, cantidadDeE);
        Assert.assertEquals(1, cantidadDeB);
        Assert.assertEquals(1, cantidadDeK);
    }
    
    @Test
    public void chequearCadenaSinImportarMayuscula() {
        ContadorLetras contadorLetras = new ContadorLetras();
        String s = "noteBOOk";
        HashMap<Character, Integer> result = contadorLetras.contar(s);
        int cantidadDeN = result.get('n');
        int cantidadDeO = result.get('o');
        int cantidadDeT = result.get('t');
        int cantidadDeE = result.get('e');
        int cantidadDeB = result.get('b');
        int cantidadDeK = result.get('k');
        
        Assert.assertEquals(6, result.size());
        Assert.assertEquals(1, cantidadDeN);
        Assert.assertEquals(3, cantidadDeO);
        Assert.assertEquals(1, cantidadDeT);
        Assert.assertEquals(1, cantidadDeE);
        Assert.assertEquals(1, cantidadDeB);
        Assert.assertEquals(1, cantidadDeK);
    }
    
    @Test
    public void chequearCadenaConTildes() {
        ContadorLetras contadorLetras = new ContadorLetras();
        String s = "Mamá";
        HashMap<Character, Integer> result = contadorLetras.contar(s);
        int cantidadDeM = result.get('m');
        int cantidadDeA = result.get('a');
        int cantidadDeAConTilde = result.getOrDefault('á', 0);
        
        Assert.assertEquals(3, result.size());
        Assert.assertEquals(2, cantidadDeM);
        Assert.assertEquals(2, cantidadDeA);
        Assert.assertEquals(0, cantidadDeAConTilde);
    }
}
