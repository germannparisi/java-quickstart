package ar.edu.untref.aydoo;

import org.junit.Test;
import org.junit.Assert;


public class FooTest 
{
    @Test
    public void doFooShouldReturnFoo()
    {
        Foo foo = new Foo();
        String result = foo.doFoo();
        Assert.assertEquals("foo", result);
    }
    
    @Test
    public void chequearPasarAMayuscula()
    {
        String s = "Un string en minúscula que pasa a mayúscula.";
        String result = s.toUpperCase();
        Assert.assertEquals("UN STRING EN MINÚSCULA QUE PASA A MAYÚSCULA.", result);
    }
    
    @Test
    public void chequearLongitud()
    {
        String s = "cadena";
        int result = s.length();
        Assert.assertEquals(6, result);
    }
    
    @Test
    public void chequearPosicion()
    {
        String s = "cadena";
        char result = s.charAt(2);
        Assert.assertEquals('d', result);
    }
    
    @Test
    public void chequearVacio()
    {
        String s = "cadena";
        boolean result = s.isEmpty();
        Assert.assertEquals(false, result);
    }
    
    @Test
    public void chequearReemplazo()
    {
        
        String s = "cadena";
        String result = s.replace('a', 'e');
        Assert.assertEquals("cedene", result);
    }
}
