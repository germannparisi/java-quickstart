package ar.edu.untref.aydoo;

import java.util.HashMap;

public class ContadorLetras {
   
    
    public HashMap<Character, Integer> contar(String x) {
        x = x.toLowerCase();
        HashMap<Character, Integer> result = new HashMap<>();
        for(int i = 0; i < x.length(); i++) {
            char c = x.charAt(i);
            if (result.containsKey(c)) {
                result.replace(c, result.get(c) + 1);
            } else {
                result.put(c, 1);
            }
        }
        return result;
    }
}
